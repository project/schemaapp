<?php

namespace Drupal\schemaapp\Cache;

/**
 * Caching for a Schema App resource.
 */
class SchemaAppCache {

  const CACHE_ID = "schemaapp_cache:";
  const APIBASE = "https://data.schemaapp.com/";

  /**
   * User's account id.
   *
   * @var string
   */
  protected $accountId;
  /**
   * Users api key.
   *
   * @var string
   */
  protected $apiKey;

  /**
   * Page URL.
   *
   * @var string
   */
  protected string $pageUrl;


  /**
   * Whether to fetch highlighter markup.
   */
  protected $useHighlighterMarkup;

  /**
   * Cache Lifetime.
   *
   * @var int
   */
  protected $cacheLifeTime;


  /**
   * Request Timeout.
   *
   * @var int
   */
  protected $requestTimeout;
  /**
   * @var string
   */
  public string $highlighterCachePrefix = '__highlighter_js';

  /**
   * @var bool
   */
  private bool $urlParams;

  /**
   * Schema App Cache.
   *
   * Cache model for a particular page,
   * uses the URL and specified config for lookup.
   *
   * @param string $pageUrl
   *   The full URL for a page including query and fragment.
   * @param array $config
   *   Array with account_id, api_key, and use_url_params options.
   */
  public function __construct(string $pageUrl, array $config) {

    $this->pageUrl = $pageUrl;

    $this->useHighlighterMarkup = $config['use_highlighter_markup'] ?? FALSE;

    $this->accountId = $config['account_id'];
    $this->apiKey = $config['api_key'];
    $this->urlParams = $config['use_url_params'] ?? FALSE;

    // Make this so we can multiply a value by up to 25%
    // This is done so not all items are expiring at the same time.
    $cacheLifeAdjuster = rand(0, 25) / 100 + 1;
    $this->cacheLifeTime = $config['cache_lifetime'] ?? 86400;
    $this->cacheLifeTime = max(1, $this->cacheLifeTime) * $cacheLifeAdjuster;

    $this->requestTimeout = $config['api_timeout'] ?? 30;
    $this->requestTimeout = max(1, $this->requestTimeout);

    $urlComponents = parse_url($this->pageUrl);

    $path = $urlComponents['path'] ?? "";

    if ($this->urlParams == FALSE) {

      $this->pageUrl = $urlComponents['scheme'] . "://" .
            $urlComponents['host'] . $path;
    }
    else {

      $query = $urlComponents['query'] ?? "";
      $querySorted = "";

      if ($query != "") {
        $queryComponents = explode("&", $query);
        sort($queryComponents);
        $querySorted = "?" . implode("&", $queryComponents);
      }

      $this->pageUrl = $urlComponents['scheme'] . "://" .
            $urlComponents['host'] . $path . $querySorted;

    }
  }

  /**
   * Get Schema Markup for the given page.
   *
   * First checks the drupal cache and returns an item there.
   * Second, if not found in the cache, search the CDN return that.
   *
   * If highlighter is selected, perform same actions on highlighter cache.
   *
   * If nothing is found false is returned.
   *
   * @return mixed
   *   Array if markup is found, false if no markup is found.
   *
   * @throws Exception
   *    On Guzzle Failure or Network failure.
   */
  public function get() {
    $schemaData = [];

    // For any markup except Highlighter
    $cacheItem = $this->checkCache();
    if ($cacheItem) {
      if (property_exists($cacheItem, "data")) {
        $schemaData[] =  $cacheItem->data;
      }
    }
    else {
      $cdnData = $this->cdnGet();
      if ($cdnData) {
        $this->setCache($cdnData);
        $schemaData[] = $cdnData;
      }
    }

    // For highlighter markup
    if ($this->useHighlighterMarkup) {
      $highlighterCacheItem = $this->checkCache($this->highlighterCachePrefix);
      if ($highlighterCacheItem) {
        if (property_exists($highlighterCacheItem, "data")) {
          $schemaData[] =  $highlighterCacheItem->data;
        }
      }
      else {
        $highlighterCdnData = $this->cdnGet($this->highlighterCachePrefix);
        if ($highlighterCdnData) {
          $this->setCache($highlighterCdnData, $this->highlighterCachePrefix);
          $schemaData[] = $highlighterCdnData;
        }
      }
    }
    else {
      // Clear highlighter cache, in the case that the 'use_highlighter_markup'
      // setting has changed.
      $this->deleteCache($this->highlighterCachePrefix);
    }

    return $schemaData ?: FALSE;
  }


  /**
   * Constructs a SchemaApp CDN URL for this Cache model's page URL.
   * Used to retrieve markup for a given page.
   *
   * @param string $cachePrefix
   *   Prefix for an encoded URL, default is empty string.
   *
   * @return string
   */
  public function getCdnUrl(string $cachePrefix = '') {
      $userGraph = str_replace('http://schemaapp.com/db/',
          "",
          $this->accountId
      );

      $encodedUrl = rtrim(base64_encode($this->pageUrl), "=");

      $apiBase = self::APIBASE . $userGraph;
      $cdnUrl = $cachePrefix
          ? $apiBase . "/$cachePrefix/$encodedUrl"
          : $apiBase . "/$encodedUrl";

      return $cdnUrl;
  }

  /**
   * CDN Get.
   *
   * Fetches directly from the Schema App CDN and does not cache.
   *
   * @param string $cachePrefix
   *   Prefix for itemUrl, default is empty string.
   *
   * @return mixed
   *   String or FALSE if no schema exists for this URL.
   */
  public function cdnGet(string $cachePrefix = '') {
    $cdnUrl = $this->getCdnUrl($cachePrefix);

    $guzzle = $this->getGuzzle();

    $cdnResponse = $guzzle->request(
      'GET', $cdnUrl, [
        'headers' => [
          'X-API-KEY' => $this->apiKey,
          'referer' => $this->pageUrl,
        ],
        'connect_timeout' => $this->requestTimeout,
      ]
    );

    $statusCode = $cdnResponse->getStatusCode();
    if ($statusCode < 200 || $statusCode > 300) {
      return FALSE;
    }

    $source = $cdnResponse->getHeader("x-amz-meta-source")[0] ?? "";
    $schemadata = $cdnResponse->getBody()->getContents();

    return $schemadata === "" ? FALSE : ["data" => $schemadata, "source" => $source];
  }

  /**
   * Check Cache.
   *
   * Checks the cache for markup for the current page.
   *
   * @param string $cachePrefix
   *   Prefix for itemUrl, default is empty string.
   *
   * @return mixed
   *   String of schema markup on success | False if it couldn't be found.
   */
  public function checkCache(string $cachePrefix = '') {
    $itemUrl = $cachePrefix . \md5($this->pageUrl);
    $item = \Drupal::cache()->get(self::CACHE_ID . $itemUrl);

    return $item;
  }

  /**
   * Set Cache.
   *
   * @param array $contents array containing [source => `source of the markup`, data =>`MARKUP`]
   *   The contents to set in the cache.
   * @param string $cachePrefix
   *   Prefix for itemUrl, default is empty string.
   * @param int $duration
   *   The period of time in seconds to cache contents.
   */
  public function setCache(array $contents, string $cachePrefix = '', $duration = NULL) {
    $itemUrl = $cachePrefix . \md5($this->pageUrl);
    $itemKey = self::CACHE_ID . $itemUrl;

    if ($duration === NULL) {
      $duration = $this->cacheLifeTime;
    }

    \Drupal::cache()->set($itemKey, $contents, $duration);
  }

  /**
   * Delete Cache.
   *
   * @param string $cachePrefix
   *   Prefix for itemUrl, default is empty string.
   */
  public function deleteCache(string $cachePrefix = '') {
    $itemUrl = $cachePrefix . \md5($this->pageUrl);
    $itemKey = self::CACHE_ID . $itemUrl;

    \Drupal::cache()->delete($itemKey);
  }

  /**
   * Return Guzzle Client from Drupal.
   *
   * @return \GuzzleHttp\Client
   *   Drupal default guzzle client.
   */
  protected function getGuzzle() {
    return \Drupal::httpClient();
  }

  /**
   * Get page URL.
   *
   * Gets the processed page URL.
   *
   * @return string
   */
  public function getPageUrl() {
    return $this->pageUrl;
  }
}
