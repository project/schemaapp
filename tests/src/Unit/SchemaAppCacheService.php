<?php

namespace Drupal\Tests\schemaapp\Unit;

use GuzzleHttp\Client;
use Drupal\schemaapp\Cache\SchemaAppCache;

/**
 * Cache Service for testing mock.
 */
class SchemaAppCacheService extends SchemaAppCache {

  /**
   * Override to return a default Guzzle Client.
   *
   * @return \GuzzleHttp\Client
   *   a guzzle client to be used for testing.
   */
  protected function getGuzzle() {
    return new Client();
  }

  /**
   * Mock function for setCache.
   *
   * Do not store anything we aren't testing drupal cache.
   *
   * @param array $contents
   *   Contents to store.
   * @param string $cachePrefix
   *   Prefix for hashedUrl, default is empty string.
   * @param int $duration
   *   The duration to store.
   *
   * @return null
   *   Leave empty for this test.
   */
  public function setCache(array $contents, string $cachePrefix = '', $duration = NULL) {
    return NULL;
  }

  /**
   * Mock function for getCache.
   *
   * Testing CDN functionality assume cache is empty.
   *
   * @param string $cachePrefix
   *   Prefix for hashedUrl, default is empty string.
   * @return null
   *   Do not check cache.
   */
  public function checkCache(string $cachePrefix = '') {
    return NULL;
  }

  /**
   * Delete Cache.
   *
   * @param string $cachePrefix
   *   Prefix for itemUrl, default is empty string.
   */
  public function deleteCache(string $cachePrefix = '') {
    return NULL;
  }

}
