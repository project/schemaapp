<?php

namespace Drupal\Tests\schemaapp\Unit;

use Drupal\Tests\UnitTestCase;
use function GuzzleHttp\json_decode;

/**
 * Testing for the Cache tool fetching from Schema App CDN.
 *
 * @group schemaapp
 */
class CacheTest extends UnitTestCase {

  /**
   * Test with URL Params.
   */
  public function testUsingParams() {
    $config = [
      'account_id' => 'DrupalEightTest',
      'api_key' => 'HLD5-AS53-LH74-MGO8',
      'use_url_params' => TRUE,
    ];

    $url = "http://drupal8.ugoziedge.com?application=drupal";

    $this->assertNotNull($url);

    $cacheService = new SchemaAppCacheService($url, $config);

    $data = $cacheService->get();

    $this->assertNotEmpty($data);

    $jsonArray = json_decode($data[0]["data"], TRUE);

    $this->assertEquals("http://schema.org", $jsonArray['@context']);
    $this->assertEquals("Editor", $data[0]["source"]);
  }

  /**
   * Test without url parameters.
   */
  public function testNoUrlParams() {
    $config = [
      'account_id' => 'DrupalEightTest',
      'api_key' => 'HLD5-AS53-LH74-MGO8',
      'use_url_params' => TRUE,
    ];

    $url = "http://drupal8.ugoziedge.com/node/1";

    $schemaCache = new SchemaAppCacheService($url, $config);

    $this->assertNotNull($schemaCache);
    $data = $schemaCache->get();
    $this->assertNotEmpty($data);

    $jsonArray = json_decode($data[0]["data"], TRUE);
    $this->assertEquals("Editor", $data[0]["source"]);

    $this->assertArrayHasKey('@type', $jsonArray);

    // Ensure content is valid.
    $this->assertEquals("Schema App", $jsonArray['name']);

    // Try the same url with params that need to be stripped.
    $noParamsConfig = [
      'account_id' => 'DrupalEightTest',
      'api_key' => 'HLD5-AS53-LH74-MGO8',
      'use_url_params' => FALSE,
    ];

    $paramsToRemoveUrl = "http://drupal8.ugoziedge.com/node/1?drupal=true";
    $schemaCacheNoParams = new SchemaAppCacheService($paramsToRemoveUrl, $noParamsConfig);

    $noParamsData = $schemaCacheNoParams->get();
    $this->assertNotNull($noParamsData);
    $jsonArrayNoParams = json_decode($noParamsData[0]["data"], TRUE);
    $this->assertEquals($jsonArray, $jsonArrayNoParams);
  }

  /**
   * Tests url param sorting.
   *
   * Tests that url params are sorted and retrieve the same item
   * no matter the order.
   */
  public function testSortedParams() {
    $config = [
      "account_id" => "DrupalEightTest",
      "api_key" => "HLD5-AS53-LH74-MGO8",
      "use_url_params" => TRUE,
    ];

    $url = "http://drupal8.ugoziedge.com?a=2&b=1";
    $unSorted = "http://drupal8.ugoziedge.com?b=1&a=2";

    $schemaCache = new SchemaAppCacheService($url, $config);

    $schemaCacheUnSorted = new SchemaAppCacheService($unSorted, $config);

    $this->assertNotNull($schemaCache);
    $this->assertNotNull($schemaCacheUnSorted);

    $data = $schemaCache->get();
    $dataUnSorted = $schemaCacheUnSorted->get();

    $this->assertNotEmpty($data);
    $this->assertNotEmpty($dataUnSorted);

    $this->assertEquals($data, $dataUnSorted);

    $wrongUrl = "http://drupal8.ugoziedge.com?a=1&b=2";

    $schemaCacheNoUrl = new SchemaAppCacheService($wrongUrl, $config);

    $this->assertNotEquals($schemaCacheNoUrl->get(), $dataUnSorted);

  }

  /**
   * Test the Guzzle Connection Timeout.
   */
  public function testConnectionTimeout() {
    $noParamsConfig = [
      'account_id' => 'DrupalEightTest',
      'api_key' => 'HLD5-AS53-LH74-MGO8',
      'use_url_params' => FALSE,
      'api_timeout' => 1,
    ];

    $paramsToRemoveUrl = "http://drupal8.ugoziedge.com/node/1?drupal=true";
    $schemaCacheNoParams = new SchemaAppCacheService($paramsToRemoveUrl, $noParamsConfig);

    $noParamsData = $schemaCacheNoParams->get();
    $this->assertNotNull($noParamsData);

  }

  /**
   * Test for multiple sources of markup, includes editor and highlighter.
   */
  public function testWithHighlighterCache() {
    // Same URL used for each cache check
    $url = 'http://drupal8.ugoziedge.com/node/1';

    // First check uses no highlighter markup
    $config = [
      'account_id' => 'DrupalEightTest',
      'api_key' => 'HLD5-AS53-LH74-MGO8',
      'use_highlighter_markup' => FALSE,
    ];

    $cache = new SchemaAppCacheService($url, $config);
    $this->assertNotNull($cache);

    $data = $cache->get();
    $this->assertNotEmpty($data);
    $this->assertCount(1, $data);

    $jsonArray = json_decode($data[0]["data"], TRUE);
    $this->assertEquals("Schema App", $jsonArray['name']);
    $this->assertEquals("Editor", $data[0]["source"]);
    // Second check uses highlighter markup
    $highlighterConfig = [
      'account_id' => 'DrupalEightTest',
      'api_key' => 'HLD5-AS53-LH74-MGO8',
      'use_highlighter_markup' => TRUE,
    ];

    $highlighterCache = new SchemaAppCacheService($url, $highlighterConfig);
    $this->assertNotNull($highlighterCache);

    $highlighterData = $highlighterCache->get();
    $this->assertNotEmpty($highlighterData);
    $this->assertCount(2, $highlighterData);

    $jsonArray = json_decode($highlighterData[0]["data"], TRUE);
    $this->assertEquals("Schema App", $jsonArray['name']);

    $highlighterJsonArray = json_decode($highlighterData[1]["data"], TRUE);
    $this->assertEquals("Highlighter Markup", $highlighterJsonArray['name']);
  }

  /**
   * Test getCdnUrl which constructs CDN urls for editor, highlighter.
   */
  public function testGetCdnUrl() {
    $url = 'http://drupal8.ugoziedge.com/node/1';
    $editorUrl = 'https://data.schemaapp.com/DrupalEightTest/aHR0cDovL2RydXBhbDgudWdvemllZGdlLmNvbS9ub2RlLzE';
    $highlightUrl = 'https://data.schemaapp.com/DrupalEightTest/__highlighter_js/aHR0cDovL2RydXBhbDgudWdvemllZGdlLmNvbS9ub2RlLzE';

    $config = [
      'account_id' => 'DrupalEightTest',
      'api_key' => 'HLD5-AS53-LH74-MGO8',
      'use_highlighter_markup' => TRUE,
    ];

    $cache = new SchemaAppCacheService($url, $config);
    $editorCdnUrl = $cache->getCdnUrl();
    $highlightCdnUrl = $cache->getCdnUrl($cache->highlighterCachePrefix);

    $this->assertEquals($editorUrl, $editorCdnUrl);
    $this->assertEquals($highlightUrl, $highlightCdnUrl);

    // Test for case with full account ID URI
    $config = [
      'account_id' => 'http://schemaapp.com/db/DrupalEightTest',
      'api_key' => 'HLD5-AS53-LH74-MGO8',
      'use_highlighter_markup' => TRUE,
    ];

    $cache = new SchemaAppCacheService($url, $config);
    $editorCdnUrl = $cache->getCdnUrl();
    $highlightCdnUrl = $cache->getCdnUrl($cache->highlighterCachePrefix);

    $this->assertEquals($editorUrl, $editorCdnUrl);
    $this->assertEquals($highlightUrl, $highlightCdnUrl);
  }

  /**
   * Test getPageURL which gets the SchemaAppCache's final/processed page URL.
   * Includes cases for URLS with use_url_params and without.
   */
  public function testGetPageUrl() {
    $url1 = 'http://drupal8.ugoziedge.com/node/1';
    $url2 = 'http://drupal8.ugoziedge.com/node/1?test=1';

    $configNoParams = [
      'account_id' => 'DrupalEightTest',
      'api_key' => 'HLD5-AS53-LH74-MGO8',
      'use_highlighter_markup' => TRUE,
      'use_url_params' => FALSE,
    ];

    $configParams = [
      'account_id' => 'DrupalEightTest',
      'api_key' => 'HLD5-AS53-LH74-MGO8',
      'use_highlighter_markup' => TRUE,
      'use_url_params' => TRUE,
    ];

    // Config doesn't use parameters, URL doesn't use parameters
    $cache = new SchemaAppCacheService($url1, $configNoParams);
    $pageUrl = $cache->getPageUrl();
    $this->assertEquals($url1, $pageUrl);
    $this->assertNotEquals($url2, $pageUrl);

    // Config doesn't use parameters, URL does use parameters
    $cache = new SchemaAppCacheService($url2, $configNoParams);
    $pageUrl = $cache->getPageUrl();
    $this->assertEquals($url1, $pageUrl);
    $this->assertNotEquals($url2, $pageUrl);

    // Config does use parameters, URL doesn't use parameters
    $cache = new SchemaAppCacheService($url1, $configParams);
    $pageUrl = $cache->getPageUrl();
    $this->assertEquals($url1, $pageUrl);
    $this->assertNotEquals($url2, $pageUrl);

    // Config and URL both use parameters
    $cache = new SchemaAppCacheService($url2, $configParams);
    $pageUrl = $cache->getPageUrl();
    $this->assertNotEquals($url1, $pageUrl);
    $this->assertEquals($url2, $pageUrl);
  }
}
