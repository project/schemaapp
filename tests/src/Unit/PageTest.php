<?php

namespace Drupal\Tests\schemaapp\Unit;
use Drupal\Core\Entity\EntityStorageException;
use Drupal\Tests\BrowserTestBase;
use Drupal\user\Entity\User;
use Exception;


class PageTest extends BrowserTestBase{
/**
 * {@inheritdoc}
 */
protected $defaultTheme = 'stark';

//  /**
//   * Modules to enable.
//   *
//   * @var array
//   */
//  protected static $modules = ['node', 'schemaapp'];
//
//  /**
//   * Theme to enable.
//   *
//   * @var string
//   */
//  protected $defaultTheme = 'stark';
//
//  /**
//   * A simple user.
//   *
//   * @var User
//   */
//  private $user;
//
//  protected function setUp(): void {
//    parent::setUp();
//
//    // Create a user.
//    $this->account = $this->drupalCreateUser(['administer rules']);
//  }
//
//
//  public function testHighlighterInjections(){
//    $this->drupalLogin($this->account);
//    $config = \Drupal::config('schemaapp.config');
//    $config = [
//      'account_id' => $config->get('account_id'),
//      'use_highlighter_markup' => $config->get('use_highlighter_markup')
//    ];
//    $config->set('use_highlighter_markup', TRUE);
//    $config->save();
//
//    $homePageHtml = $this->drupalGet("");
//    echo "\n $homePageHtml";
//
//    $this->assertNotNull($homePageHtml);
//
//    $injection1 = "<script>window.schema_highlighter={accountId: '" .
//      str_replace('http://schemaapp.com/db/', '',
//        $config['account_id'])
//                    . "', output: false}</script>";
//    $injection2 = "<script async=\"1\" src=\"https://cdn.schemaapp.com/javascript/highlight.js\"></script>";
//
//    $this->assertTrue(stripos($homePageHtml, $injection1));
//    $this->assertTrue(stripos($homePageHtml, $injection2));
//  }
}
