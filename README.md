# INTRODUCTION

The Schema App Drupal Module adds server caching for schema resources. 
It provides a 24 cycle for cached resources allowing them to be refreshed 
within Schema App. 

This module requires a current active Schema App subscription to use, 
to learn more about Schema App visit: https://www.schemaapp.com

# REQUIREMENTS

No special requirements

# RECOMMENDED MODULES

No special requirements

# INSTALLATION

* Install as you would normally install a contributed Drupal module. Visit
   https://www.drupal.org/docs/8/extending-drupal-8/installing-drupal-8-modules
   for further information.

# CONFIGURATION

* Configure the user permissions in 
   Configuration >> System >> Schema App module settings

Users will copy their API Key and Account ID from:
 https://app.schemaapp.com/integration/javascript

Users will then choose if they want to use get parameters in their URL 
for resources, in most cases they will not want this option. 

For example if using get parameters if the following url is visited: 
http://example.com/index?source=mail1527gk5h2 
and markup is deployed to: http://example.com/index it will not match, 
Schema App will treat the entire URL as unique. 
Users should set this option unless the get parameter significantly 
changes the page. 

# FAQ

Q: Why isn't my Schema Markup changing after updating?

A: The local caching of the module refreshes once every 24 hours 
so you will see the change go live after the plugin refreshes again. 
This is the intended behaviour.

Q: Why isn't my Highlighter JS markup included after enabling it?

A: When enabling highlighter markup for the first time, you may need to 
clear your cache in Drupal.