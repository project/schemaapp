<?php

/**
 * @file
 * Implements local caching of Schema Markup from Schema App.
 */

use GuzzleHttp\Exception\ServerException;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\schemaapp\Cache\SchemaAppCache;

/**
 * Schema App Help.
 *
 * Implements hook_help().
 *
 * Displays README.md in markdown or plain text.
 *
 * @param string $route_name
 *   String of route name.
 * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
 *   Drupal route match interface.
 *
 * @return mixed
 *   string | NULL
 */
function schemaapp_help($route_name, RouteMatchInterface $route_match) {

  switch ($route_name) {
    case 'help.page.schemaapp':
      $text = file_get_contents(dirname(__FILE__) . '/README.md');
      if (!\Drupal::moduleHandler()->moduleExists('markdown')) {
        return '<pre>' . $text . '</pre>';
      }
      else {
        // Use the Markdown filter to render the README.
        $filter_manager = \Drupal::service('plugin.manager.filter');
        $settings = \Drupal::configFactory()
          ->get('markdown.settings')
          ->getRawData();
        $config = ['settings' => $settings];
        $filter = $filter_manager->createInstance('markdown', $config);
        return $filter->process($text, 'en');
      }
  }
  return NULL;
}

/**
 * Schema Page Attachments Alter.
 *
 * Adds Schema Markup to public pages where applicable markup is found
 * in the local cache or on SchemaApp CDN. Markup is inserted into the head
 * tag within a <script type="application/ld+json> tag that contains the markup
 * Using JSON-LD is Google preferred format:
 * https://www.searchenginejournal.com/google-structured-data-preference/297479/
 *
 * @param array $page
 *   Drupal page.
 */
function schemaapp_page_attachments_alter(array &$page) {
  // Skip pages that have exceptions.
  if (\Drupal::request()->get('exception')) {
    return;
  }

  // Schema does not need to deployed to admin pages, only crawable pages.
  if (\Drupal::service('router.admin_context')->isAdminRoute() === FALSE) {

    // Build the full URL with path, query and fragments.
    $schemaHost = \Drupal::request()->getSchemeAndHttpHost();
    $path = \Drupal::request()->getRequestUri();
    $currentUrl = $schemaHost . $path;

    // Get the user's current config settings.
    $config = \Drupal::config('schemaapp.config');
    $config = [
      'account_id' => $config->get('account_id'),
      'api_key' => $config->get('api_key'),
      'use_url_params' => $config->get('use_url_params'),
      'use_highlighter_markup' => $config->get('use_highlighter_markup'),
      'enable_debug_info' => $config->get('enable_debug_info'),
      'cache_lifetime' => $config->get('cache_lifetime'),
      'api_timeout' => $config->get('api_timeout'),
    ];

    $cacheService = new SchemaAppCache($currentUrl, $config);
    $transformedUrl = $cacheService->getPageUrl();
    try {
      // Add some debugging information to the <head> as <meta> elements
      // If enable_debug_info is set, or if the 'schema-app-debug=true' parameter is present
      if ($config['enable_debug_info'] || \Drupal::request()->query->get('schema-app-debug')) {
        $requestUrl = [
          '#tag' => 'meta',
          '#attributes' => [
            'name' => 'schema-app-debug-request-url',
            'content' => $currentUrl,
          ],
        ];
        $page['#attached']['html_head'][] = [$requestUrl, 'schema-app-debug-request-url'];

        $cachePageUrl = [
          '#tag' => 'meta',
          '#attributes' => [
            'name' => 'schema-app-debug-cache-page-url',
            'content' => $cacheService->getPageUrl(),
          ],
        ];
        $page['#attached']['html_head'][] = [$cachePageUrl, 'schema-app-debug-cache-page-url'];

        $accountIdMeta = [
          '#tag' => 'meta',
          '#attributes' => [
            'name' => 'schema-app-debug-account-id',
            'content' => $config['account_id'],
          ],
        ];
        $page['#attached']['html_head'][] = [$accountIdMeta, 'schema-app-debug-account-id'];

        $cdnUrlMeta = [
          '#tag' => 'meta',
          '#attributes' => [
            'name' => 'schema-app-debug-cdn-url',
            'content' => $cacheService->getCdnUrl(),
          ],
        ];
        $page['#attached']['html_head'][] = [$cdnUrlMeta, 'schema-app-debug-cdn-url'];

        $highlightJsCdnUrlMeta = [
          '#tag' => 'meta',
          '#attributes' => [
            'name' => 'schema-app-debug-highlight-js-cdn-url',
            'content' => $cacheService->getCdnUrl($cacheService->highlighterCachePrefix),
          ],
        ];
        $page['#attached']['html_head'][] = [$highlightJsCdnUrlMeta, 'schema-app-debug-highlight-js-cdn-url'];
      }

      if ($config['use_highlighter_markup']){
        $highlight_js = [
          '#tag' => 'script',
          '#value' => "window.schema_highlighter={accountId: '" .
            str_replace('http://schemaapp.com/db/', '',
              $config['account_id']) . "', output: false}"
        ];
        $page['#attached']['html_head'][] = [$highlight_js, 'schema-app-highlight-js_key'];

        $highlight_script_js = [
          '#tag' => 'script',
          '#value' => '',
          '#attributes' => [
            "async" => TRUE,
            "src" => "https://cdn.schemaapp.com/javascript/highlight.js"
          ]
        ];
        $page['#attached']['html_head'][] = [$highlight_script_js, 'schemaapp_highlight_script_js_key'];
      }

      $schemaData = $cacheService->get();
      // Get will return false if both the cache and cdn do not contain schema
      // for this particular page.
      if ($schemaData === FALSE) {
        return;
      }

      $markupIndex = 0;
      foreach ($schemaData as $schema) {
        // Create a json-ld script tag to contain the schema markup.
        $viewport = [
          '#type' => 'html_tag',
          '#tag' => 'script',
          '#value' => $schema['data'],
          '#attributes' => [
            'type' => 'application/ld+json',
          ],
        ];

        if (isset($schema['source']) && $schema['source'] != "") {
          // Source is set
          $viewport['#attributes']['data-source'] = $schema['source'];
        }

        $page['#attached']['html_head'][] = [$viewport, "schemaapp-$markupIndex"];
        $markupIndex++;
      }

      // When config is changed, we want to invalidate the cache
      // For example, when the highlighter setting is toggled
      $page['#cache']['tags'][] = 'config:schemaapp.config';
    }
    catch (ServerException $guzzleException) {
      $guzzleMessage = $guzzleException->getMessage();
      \Drupal::logger('schemaapp')
        ->warning("Lookup for $currentUrl transformed as $transformedUrl failed due to guzzle error: $guzzleMessage");

      return FALSE;
    }

    catch (\Exception $general) {
      $generalMessage = $general->getMessage();
      \Drupal::logger('schemaapp')
        ->warning("Lookup for $currentUrl transformed as $transformedUrl failed due to error: $generalMessage");

      return FALSE;
    }

  }
}
